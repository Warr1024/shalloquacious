/*
 * ======================================================================
 * Copyright (c) 2016 Warr1024 <warr1024@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * ======================================================================
 */

#include "markov.h"
#include "markov_data.h"

#include <math.h>

double markov_score(const char *onion) {
	double p = 1;
	int idx = 0, i = 0, t = 0;
	for(i = 0; onion[i]; i++) {
		t = onion[i];
		t -= 'a';
		if((t < 0) || (t > 25))
			return 0;
		idx = (idx % (27 * 27 * 27)) * 27 + t + 1;
		p *= markov_data[idx];
		if(p <= 0)
			return 0;
	}
	idx = (idx % (27 * 27 * 27)) * 27;
	p *= markov_data[idx];
	if(p <= 0)
		return 0;
	return -log(p);
}
